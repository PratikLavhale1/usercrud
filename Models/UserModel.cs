﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcCrudWithAdo.Models
{
    public class UserModel
    {
        [Display(Name = "User ID")]
        public int Id { get; set; }
        [Required(ErrorMessage ="Please enter Name")]
        [Display (Name="User Name")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please enter Email")]
        [EmailAddress(ErrorMessage = "Enter Valid Email")]
        [Display(Name="User Email")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Please Enter Age")]
        [Display(Name="User Age")]
        [Range(18,60,ErrorMessage ="Age must in between 18 to 60")]
        public int age { get; set; }

    }
}