﻿using MvcCrudWithAdo.Models;
using MvcCrudWithAdo.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcCrudWithAdo.Controllers
{
    public class UserController : Controller
    {
        UserDAL userdal=new UserDAL();
        // GET: User
        public ActionResult List()
        {
            var data=userdal.GetUsers();
            return View(data);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(UserModel user)
        {
            if(userdal.InsertUser(user))
            {
                TempData["InsertMsg"] = "<script>alert('User Saved Successful')</script>";
                return RedirectToAction("List");
            }
            else
            {
                TempData["InsertErrorMsg"] = "<script>alert('Data not saved')</script>";
            }
            return View();
        }

        public ActionResult Details(int id)
        {
            var data = userdal.GetUsers().Find(a => a.Id == id);
            return View(data);
        }
        public ActionResult Edit(int id)
        {
            var data=userdal.GetUsers().Find(a=>a.Id==id);
            return View(data); 
        }

        [HttpPost]
        public ActionResult Edit(UserModel user)
        {
            if (userdal.UpdateUser(user))
            {
                TempData["UpdateMsg"] = "<script>alert('User Updated Successful')</script>";
                return RedirectToAction("List");
            }
            else
            {
                TempData["UpdateErrorMsg"] = "<script>alert('Data not Updated')</script>";
            }
            return View();
        }

       // [HttpPost]
        public ActionResult Delete(int id)
        {
            int r = userdal.DeleteUser(id);
            if (r>0)
            {
                TempData["DeleteMsg"] = "<script>alert('User Deleted Successful')</script>";
                return RedirectToAction("List");
            }
            else
            {
                TempData["DeleteErrorMsg"] = "<script>alert('Data not Deleted')</script>";
            }
            return View();
        }
    }
}